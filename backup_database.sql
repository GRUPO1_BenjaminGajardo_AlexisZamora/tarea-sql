--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.4

-- Started on 2022-11-11 20:09:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 16610)
-- Name: board; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.board (
    id integer NOT NULL,
    name character varying(60) NOT NULL
);


ALTER TABLE public.board OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16609)
-- Name: board_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.board_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.board_id_seq OWNER TO postgres;

--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 211
-- Name: board_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.board_id_seq OWNED BY public.board.id;


--
-- TOC entry 216 (class 1259 OID 16638)
-- Name: post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.post (
    idpost integer NOT NULL,
    message character varying(60) NOT NULL,
    created_at date NOT NULL,
    updated_at date,
    updated_by integer,
    created_by integer NOT NULL,
    topic integer NOT NULL
);


ALTER TABLE public.post OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16637)
-- Name: post_idpost_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.post_idpost_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_idpost_seq OWNER TO postgres;

--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 215
-- Name: post_idpost_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.post_idpost_seq OWNED BY public.post.idpost;


--
-- TOC entry 214 (class 1259 OID 16619)
-- Name: topics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.topics (
    idtopic integer NOT NULL,
    subjet character varying(60) NOT NULL,
    last_update date NOT NULL,
    board integer NOT NULL,
    starter integer NOT NULL
);


ALTER TABLE public.topics OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16618)
-- Name: topics_idtopic_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.topics_idtopic_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.topics_idtopic_seq OWNER TO postgres;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 213
-- Name: topics_idtopic_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.topics_idtopic_seq OWNED BY public.topics.idtopic;


--
-- TOC entry 210 (class 1259 OID 16582)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    iduser integer NOT NULL,
    username character varying(60) NOT NULL,
    password character varying(60) NOT NULL,
    email character varying(255) NOT NULL,
    is_superuser bit(1) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16581)
-- Name: users_iduser_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_iduser_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_iduser_seq OWNER TO postgres;

--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 209
-- Name: users_iduser_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_iduser_seq OWNED BY public.users.iduser;


--
-- TOC entry 3182 (class 2604 OID 16613)
-- Name: board id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.board ALTER COLUMN id SET DEFAULT nextval('public.board_id_seq'::regclass);


--
-- TOC entry 3184 (class 2604 OID 16641)
-- Name: post idpost; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post ALTER COLUMN idpost SET DEFAULT nextval('public.post_idpost_seq'::regclass);


--
-- TOC entry 3183 (class 2604 OID 16622)
-- Name: topics idtopic; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topics ALTER COLUMN idtopic SET DEFAULT nextval('public.topics_idtopic_seq'::regclass);


--
-- TOC entry 3181 (class 2604 OID 16585)
-- Name: users iduser; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN iduser SET DEFAULT nextval('public.users_iduser_seq'::regclass);


--
-- TOC entry 3346 (class 0 OID 16610)
-- Dependencies: 212
-- Data for Name: board; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.board (id, name) FROM stdin;
1	Universidad
2	Trabajo
3	Ocio
\.


--
-- TOC entry 3350 (class 0 OID 16638)
-- Dependencies: 216
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.post (idpost, message, created_at, updated_at, updated_by, created_by, topic) FROM stdin;
1	Se inicio la tarea SQL	2022-11-11	2022-11-11	1	1	1
2	Se estan creando las tablas	2022-11-11	2022-11-11	2	2	1
3	Se esta probando el funcionamiento	2022-11-11	2022-11-11	1	1	1
6	Pruebas posteriores	2022-11-13	2022-11-13	1	1	1
7	Fin de semestre	2022-11-15	2022-11-15	2	2	1
9	Todo funciona correcto	2022-11-12	2022-11-14	1	1	1
11	Tia portal en PLC	2022-11-08	\N	\N	1	2
10	Estudiando para SCADA	2022-11-08	\N	\N	1	3
4	post creado por pepito	2022-11-12	\N	\N	3	1
5	post creado por pablito	2022-11-12	\N	\N	4	1
12	Estamos preparando el informe modulo 2	2022-11-10	\N	\N	2	4
13	programando en ARM	2022-12-10	\N	\N	2	5
\.


--
-- TOC entry 3348 (class 0 OID 16619)
-- Dependencies: 214
-- Data for Name: topics; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.topics (idtopic, subjet, last_update, board, starter) FROM stdin;
1	EIE507	2022-11-11	1	1
2	EIE603	2022-11-11	1	1
3	EIE606	2022-11-11	1	1
4	EIE437	2022-11-11	1	2
5	EIE439	2022-11-11	1	2
\.


--
-- TOC entry 3344 (class 0 OID 16582)
-- Dependencies: 210
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (iduser, username, password, email, is_superuser) FROM stdin;
1	Benjamin	Admin.123	benjamin.gajardo.e@mail.pucv.cl	1
2	Alexis	Admin.123	alexis.zamora.b@mail.pucv.cl	1
3	Pepito	clave123	pepito@gmail.com	0
4	Pablito	clavesegura123	pablito@gmail.com	0
\.


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 211
-- Name: board_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.board_id_seq', 3, true);


--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 215
-- Name: post_idpost_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.post_idpost_seq', 13, true);


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 213
-- Name: topics_idtopic_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.topics_idtopic_seq', 6, true);


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 209
-- Name: users_iduser_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_iduser_seq', 4, true);


--
-- TOC entry 3190 (class 2606 OID 16617)
-- Name: board board_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.board
    ADD CONSTRAINT board_name_key UNIQUE (name);


--
-- TOC entry 3192 (class 2606 OID 16615)
-- Name: board board_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.board
    ADD CONSTRAINT board_pkey PRIMARY KEY (id);


--
-- TOC entry 3198 (class 2606 OID 16643)
-- Name: post post_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (idpost);


--
-- TOC entry 3194 (class 2606 OID 16624)
-- Name: topics topics_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topics
    ADD CONSTRAINT topics_pkey PRIMARY KEY (idtopic);


--
-- TOC entry 3196 (class 2606 OID 16626)
-- Name: topics topics_subjet_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topics
    ADD CONSTRAINT topics_subjet_key UNIQUE (subjet);


--
-- TOC entry 3186 (class 2606 OID 16587)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (iduser);


--
-- TOC entry 3188 (class 2606 OID 16589)
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- TOC entry 3202 (class 2606 OID 16649)
-- Name: post post_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.users(iduser);


--
-- TOC entry 3203 (class 2606 OID 16654)
-- Name: post post_topic_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_topic_fkey FOREIGN KEY (topic) REFERENCES public.topics(idtopic);


--
-- TOC entry 3201 (class 2606 OID 16644)
-- Name: post post_updated_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_updated_by_fkey FOREIGN KEY (updated_by) REFERENCES public.users(iduser);


--
-- TOC entry 3199 (class 2606 OID 16627)
-- Name: topics topics_board_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topics
    ADD CONSTRAINT topics_board_fkey FOREIGN KEY (board) REFERENCES public.board(id);


--
-- TOC entry 3200 (class 2606 OID 16632)
-- Name: topics topics_starter_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topics
    ADD CONSTRAINT topics_starter_fkey FOREIGN KEY (starter) REFERENCES public.users(iduser);


-- Completed on 2022-11-11 20:09:21

--
-- PostgreSQL database dump complete
--

